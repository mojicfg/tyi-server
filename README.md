# Bizzare Pizza API Server

This is the API Server for «Bizzare Pizza» Project. It handles the café system & answers HTTP queries.

## Technologies used

* Language: Python3
* Framework: Flask

## Installation guide

Installing our project is as simple as follows:
```console
$ cd ~/Downloads  # navigate to your Downloads directory
$ curl https://gitlab.com/bizzare-pizza/bizzare-api/raw/master/resources/misc/install.sh > install.sh
 # download our installation script
$ cd ~/your/import/directory  # navigate to where you want
$ bash ~/Downloads/install.sh  # execute the script
```

Some more useful commands:
```console
$ source ~/your/import/directory/bizzare-api-env/bin/activate # enter our environment
$ ./run.sh --load-db # run our project
$ deactivate # exit our environment
```

## Project structure

|     File    |        Purpose       |
|:-----------:|:--------------------:|
|   main.py   |  The main app file   |
| database.py | Database declaration |

## Codestyle rules

The important bits of code which should be revised later are marked with 'WARNING' as follows:
```
some code here # WARNING : some notes (optional)
```

